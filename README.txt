See wiki page at https://bitbucket.org/camlost/searchmaillogs/wiki.

Usage:
    python searchmail.py -d DATE_START [-e DATE_END] [-s SENDER] [-r RECIPIENT] [-i MESSAGE_ID] [-o CSV_FILE] [-v]
	
    Parameters:
		-d DATE_START Date (YYYYMMDD) when the message was sent.
		-e DATE_END   Date (YYYYMMDD) when to finish searching.
		-s SENDER     E-mail address. Search only for such messages that were sent by the SENDER.
		-r RECIPIENT  E-mail address. Search only for such messages that were sent to this RECIPIENT.
		-i MESSAGE_ID Search for a message with the specified Messages-ID.
		-o CSV_FILE   Write output to CSV file
		-v            Turn verbose output on.
		-h            Display help.
		
    Example:
    	python searchmail.py -d 20141125 -s sender@domain.com -r recipient@example.net -v