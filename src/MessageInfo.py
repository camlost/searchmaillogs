import datetime
from postfixloglib.loglinedata import LogDataType

# Default time format in postfix logs is: Nov 11 14:56:00
LOG_TIME_FORMAT = "%b %d %H:%M:%S"

class MessageInfoDelivery:
	'''
	Information about message delivery attemt.
	Contains data about time, recipient, target SMTP server and delivery status.
	'''
	
	def __init__(self, lst_str):
		self.time = datetime.datetime.strptime(lst_str[0], LOG_TIME_FORMAT).strftime("%m/%d %H:%M:%S")
		self.rcpt = lst_str[1].lower()
		self.relay = lst_str[2]
		self.dsn = lst_str[3]
		self.status = lst_str[4]
		self.response = lst_str[5]
		self.delay = lst_str[6]
		
	def __str__(self):
		result = "{0} To:{1} -> {2} ({3} - {4})".format(self.time, self.rcpt, self.relay, self.dsn, self.status)
		return result
	
	def output(self, verbose = False):
		result = "{0} To:{1} -> {2} ({3})".format(self.time, self.rcpt, self.relay, self.dsn)
		if verbose:
			result += "\n    {0}".format(self.status)
		return result

class MessageInfo:
	'''
	Message data.
	Time when a message was received, internal (queue) ID, message ID, sender, size,
	sending server ('SMTP client'), array of delivery attempts, etc.
	'''
	def __init__(self, int_id):
		self.int_id = int_id
		self.msg_id = ""
		self.sender = ""
		self.delivery = []
		self.delay = 0
		self.size = ""
		self.time_received = 0
		self.client_host = ""
		self.in_progress = True
		self.rcpts = []
		self.virus = ""
		self.virus_flags = ""
		self._import_phase = 0
		self.nextQID = None
	
	def __str__(self):
		result = "[{3}] FROM: {0}, TO: {1}, MSGID: {2}, SIZE: {4}".format(self.sender, self.rcpts, self.msg_id, self.int_id, self.size)
		return result
	
	def getAsArray(self):
		result = []
		for item in self.delivery:
			row = [
				item.time,
				item.dsn,
				self.sender,
				item.rcpt,
				self.msg_id,
				self.int_id,
				self.size,
				len(self.rcpts),
				self.time_received,
				self.delay,
				self.client_host,
				item.relay,
				item.status,
				item.response
				]
			result.append(row)
		return result
		
	def isMatchingFilter(self, filter_obj):
		result = (self.nextQID == None)
		if filter_obj.message_id:
			result = result and (filter_obj.message_id == self.msg_id)
		if filter_obj.sender:
			result = result and (self.sender.find(filter_obj.sender.lower()) > -1)
		if filter_obj.rcpt:
			rcpts_str = ",".join(self.rcpts)
			result = result and (rcpts_str.find(filter_obj.rcpt.lower()) > -1)
		if filter_obj.client:
			result = result and (self.client_host.lower().find(filter_obj.client) > -1)
		return result
		
	def printData(self, rcpt_addr = None, verbose = False):
		print("Internal ID: {0}".format(self.int_id))
		print("Client host: {0}".format(self.client_host))
		print("Received at: {0}".format(self.time_received))
		print("Message ID:  {0}".format(self.msg_id))
		print("From:        {0}".format(self.sender))
		print("Delay:       {0}".format(self.delay))
		print("Virus found: {0}".format(self.virus))
		print("Virus flags: {0}".format(self.virus_flags))
		print("Delivery:")
		
		self.delivery.sort(key=lambda x: x.time, reverse=False)
		for item in self.delivery:
			if rcpt_addr == None or item.rcpt.find(rcpt_addr.lower()) > -1:
				print("  {0}".format(item.output(verbose)))
		print("Queue REMOVE flag hit: {0}".format(not self.in_progress))
		
	def setReceiveInfo(self, client_host, sender, size):
		self.client_host = client_host
		self.sender = sender
		self.size = size
		
	def update(self, log_data):
		if log_data.data_type == LogDataType.CLIENT:
			self.client_host = log_data.client_host.lower()
			self.time_received = datetime.datetime.strptime(log_data.log_time, LOG_TIME_FORMAT).strftime("%m/%d %H:%M:%S")
		elif log_data.data_type == LogDataType.MESSAGEID:
			self.msg_id = log_data.message_id
		elif log_data.data_type == LogDataType.FROM:
			self.sender = log_data.sender.lower()
			self.size = log_data.size
			# log_data.rcptCount not used though available
		elif log_data.data_type == LogDataType.AMAVIS:
			# TODO: store virus name only in case of infected messages
			self.virus = log_data.virus_found
			self.virus_flags = log_data.virus_flags
		elif log_data.data_type == LogDataType.TO:
			self.rcpts.append(log_data.rcpt.lower())
			delivery = MessageInfoDelivery([log_data.log_time, log_data.rcpt, log_data.relay, log_data.dsn, log_data.status, log_data.response, log_data.delay])
			self.delivery.append(delivery)
		elif log_data.data_type == LogDataType.REMOVED:
			self.in_progress = False
