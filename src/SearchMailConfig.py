import json
import os

class SearchMailConfig:
	'''
	Simple class for storing application configuration.
	'''
	
	def __init__(self):
		self.log_path = "/var/log"
		self.log_file = "mail.log"
		
	def load(self, path):
		result = False
		if os.path.isfile(path):
			fp = open(path, "r")
			data = json.load(fp)
			fp.close()
			self.log_path = data["log_path"]
			self.log_file = data["log_file"]
			result = True
		return result
	
	def save(self, path):
		fd = open(path, "w")
		json.dump(self.__dict__, fd, indent = 4)
		fd.close()
