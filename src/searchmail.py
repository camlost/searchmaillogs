#!/usr/bin/env python3

import argparse
import csv
import datetime
import os.path
import time

import MessageInfo
import SearchMailConfig

from postfixloglib.logparser import LogParser
from postfixloglib.loglinedata import LogDataType

VERSION = "4.0"

# Folder paths where the config file is expected
CFG_FILE_PATH = [
	"./searchmail.conf", 
	"/usr/local/etc/searchmail.conf"
	]

CSV_FILE_HEADER = [
	"Time-Sent",
	"DSN",
	"Sender",
	"Recipient",
	"Message-Id",
	"Internal-Id",
	"Size",
	"Rcpt-Count",
	"Time-Rcv",
	"Delay",
	"Client-Host",
	"Relay-Host",
	"Status",
	"Response"
	]

def main():
	print("SearchMailLog v" + VERSION)
	time_start = time.time()

	parser = argparse.ArgumentParser(prog = "SearchMailLog", description = "Search postfix log files for messages.")
	parser.add_argument("-d", "--date", default = datetime.date.today().strftime("%Y%m%d"), help = "date (YYYYMMDD) when to search for messages")
	parser.add_argument("-e", "--end-date", help = "last date (YYYYMMDD) when to search for messages")
	parser.add_argument("-i", "--message-id", help = "message id")
	parser.add_argument("-s", "--sender", help = "message sender")
	parser.add_argument("-r", "--rcpt", help = "message recipient")
	parser.add_argument("-c", "--client", help = "client host name or IP address")
	parser.add_argument("-v", "--verbose", help = "turn verbose mode on", action = "store_true")
	grp = parser.add_mutually_exclusive_group()
	grp.add_argument("-o", "--output", help = "write (append) result to CSV file")
	grp.add_argument("-O", "--overwrite", help = "save result to CSV file; overwrite file")
	args = parser.parse_args()
	
	# Read settings from file or use default values if file not found
	cfg = SearchMailConfig.SearchMailConfig()
	cfg_found = False
	for path in CFG_FILE_PATH:
		cfg_found = cfg.load(path)
		if cfg_found:
			break
	
	parseLogFiles(cfg, args)
	
	# Save configuration to file
	if not cfg_found:
		try:
			cfg.save(CFG_FILE_PATH[0])
		except Exception:
			print("Warning: Unable to save configuration to {0}".format(CFG_FILE_PATH[0]))
			
	if args.verbose:
		print("Execution time: {0} s".format(int(time.time() - time_start)))

def openTargetCsv(args):
	'''
	Open target CSV file as necessary (in overwrite or append mode).
	
	Arguments:
		- path: Path to CSV file
		- args: Program arguments (object provided by the argparse module)
	
	Return:
		csvwriter object or None if output to file not requested
	'''
	result = None
	path = args.overwrite if args.overwrite else args.output
	
	if path:
		csv_header = None
		# Is new file necessary? (either overwrite is set, or file doesn't exist)
		if args.overwrite != None or not os.path.isfile(path):
			csv_header = CSV_FILE_HEADER
			
		try:
			file_mode = "w" if args.overwrite else "a"
			fp = open(path, file_mode, newline = "")
			result = csv.writer(fp, delimiter = ",", quoting = csv.QUOTE_NONNUMERIC)
			if csv_header != None:
				result.writerow(csv_header)
		except Exception as err:
			print("ERROR: Unable to create output file {0}.\n{1}".format(path, err))
	return result

def parseLogFiles(cfg, args):
	"""
	Parse log files.
	Collect data from log files and generate output - either to a file, or to stdout.
	
	Arguments:
		- cfg: Application configuration (instance of SearchMailConfig)
		- args: Application arguments (object provided by the argparse module)
	"""
	

	def process_log_data(log_data):
		'''
		Handle log data which were read from a log line.
		Callback function - called by postfixloglib.
		'''
		
		queue_id = log_data.qid
		
		# Get MessageInfo object - either a new one, or one stored in data
		msg_info = None
		if queue_id not in data:
			msg_info = MessageInfo.MessageInfo(queue_id)
			data[queue_id] = msg_info
		else:
			msg_info = data[queue_id]
		msg_info.update(log_data)
		
		# In case of Amavis log line overwrite some msg_info members
		if log_data.data_type == LogDataType.AMAVIS and log_data.source_qid in data:
			src_info = data[log_data.source_qid]
			msg_info.setReceiveInfo(src_info.client_host, src_info.sender, src_info.size)
			src_info.nextQID = queue_id
		
		# In case of 'REMOVE' event output data and free memory
		if log_data.data_type == LogDataType.REMOVED:
			# Output data matching filter in case of LogLineType.Remove.
			# Objects with valid nextQID must not be displayed.
			if msg_info.isMatchingFilter(args):
				write_output(msg_info, csv, args)
			# Remove object from data (= free memory)
			del data[queue_id] # faster then data.pop(queue_id, None)
	
	csv = openTargetCsv(args)
	
	data = {}
	parser = LogParser()
	parser.parseLog(cfg.log_path, cfg.log_file, args.date, args.end_date, process_log_data, verbose = args.verbose)
			
	# Write content of data (= incomplete delivery attempts) to output.
	for key in data:
		msg_info = data[key]
		if msg_info.isMatchingFilter(args):
			write_output(msg_info, csv, args)
	
	# The CSV file will be closed automagically.
	del csv

def write_output(info, csv, args):
	"""
	Write log data to output - either to stdout, or to CSV file if specified.
	
	Arguments:
		- info: MessageInfo instance
		- fp: CSV file pointer (or None)
		- args: Application arguments (object provided by the argparse module)
	"""
	
	if (csv):
		msg_data_lst = info.getAsArray()
		for row in msg_data_lst:
			rcpts_lst = ",".join(info.rcpts)
			if args.rcpt == None or rcpts_lst.find(args.rcpt.lower()) > -1:
				csv.writerow(row)
	else:
		info.printData()
	
if __name__ == "__main__":
	main()
